package com.example.newsappt1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.util.Log
import com.bumptech.glide.Glide

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val imageView = findViewById<ImageView>(R.id.newsImage)
        Glide
            .with(this)
            .load("https://i2.wp.com/electrek.co/wp-content/uploads/sites/3/2019/02/Powerwall-and-Backup-Gateway-2-e1550862732928.png?resize=1200%2C628&quality=82&strip=all&ssl=1")
            .placeholder(R.drawable.ic_no_image)
            .into(imageView);
    }
}