package com.example.newsappt1

fun main() {
    //Instatiate class DataBase
    var db = DataBase()

    //Add a few contacts to the DB
    db.add(Contact(firstName = "Gabriel",lastName = "Nazato",nickName = "sbrug",phoneNumber = "3929",id = 1))
    db.add(Contact(firstName = "Renato",lastName = "Costa",phoneNumber = "1234",id = 2))
    db.add(Contact(firstName = "Flavia",lastName = "Carvalho",phoneNumber = "4321",id = 3))
    db.add(Contact(firstName = "Robson",lastName = "Teles",phoneNumber = "2323"))

    //Print Contacts by Id
    db.printByid(1)
    db.printByid(4)

    //Update contact and print again
    db.update(1, Contact(firstName = "Gabriel",lastName = "Naza",nickName = "sbrug",phoneNumber = "3929",id = 1))
    db.printByid(1)

    //Delete contact
    db.delete(2)
}

data class Contact(val firstName:String, val lastName:String, val nickName:String? = "", val phoneNumber:String, var id:Int? = null)


class DataBase() {
    private val contacts: MutableMap<Int,Contact> = mutableMapOf()

    fun add(contact: Contact) {
        if(contact.id == null){
            val maxkey:Int = if(contacts.keys.max() == null) 0 else contacts.keys.max()!!
            println(maxkey)
            contact.id = maxkey + 1
        }

        contacts.put(contact.id!!, contact)
    }

    fun update(id:Int, contact:Contact) {
        contacts.set(id, contact)
    }

    fun returnAll():MutableMap<Int,Contact> {
        return contacts
    }

    fun returnById(id:Int):Contact? {
        return contacts.get(id)
    }

    fun delete(id:Int) {
        contacts.remove(id)
        println("- - - - - - - - - -")
        println("Contact (id:$id) deleted")
    }

    fun printByid(id:Int){
        val con = returnById(id)

        println("- - - - - - - - - -")

        if(con == null) {
            println("id: $id")
            println("Contact not found!")
        }
        else{
            println("id: ${con?.id}")
            println("Name: ${con?.firstName} ${con?.lastName}")
            println("Nickname: ${con?.nickName}")
            println("Phone: ${con?.phoneNumber}")
        }
    }
}